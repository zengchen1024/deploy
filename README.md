# deploy

#### 如果配置
修改deploy.yaml，按如下格式添加配置，即可支持将一个模型部署到另一个云。当前支持的云包括：天翼云。

``` yaml
xxx/xxx:                    # 模型Id，格式是{owner}/{name}
- cloud: xxx                # 云平台的名称
  icon: icon/xxx.png        # 云平台的图标
  link:  xxx                # 云平台部署模型的链接地址
  desc: deploy with xxx     # 功能说明（英文）
  desc_cn: 部署到xxx         # 功能说明（中文）
```

